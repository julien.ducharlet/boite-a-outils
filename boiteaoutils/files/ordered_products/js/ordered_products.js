var tokenAdminOrders;
var tokenAdminProducts;

/**
 * public aOfFilms is used to store all datas of movies
 * @var array
 */
var aOfReferences = [];

// tableau contenant les commandes
var aOfOrders = [];

// variable contenant la datatable
var tablesOrders;

var submitted = 0;

function loadTokenAdminOrders() {
    var datas = {
        path : "token",
        page : "get_token_admin_orders",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            tokenAdminOrders = result;
        })
        .fail(function(err) {
            console.log("error");
            // error
        });
}

function loadTokenAdminProducts() {
    var datas = {
        path : "token",
        page : "get_token_admin_products",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            var linkAdminProducts = result;
            var AdminProductsParts = linkAdminProducts.split('token=', 2);
            tokenAdminProducts = AdminProductsParts[1];
        })
        .fail(function(err) {
            console.log("error");
            // error
        });
}

/**
 * Get Movies from database
 *
 * if OK add movies to array aOfFilms
 *
 * if OK then build table and call datatable
 */
function loadReferences() {
    $('#divModalLoading').show();
    var datas = {
        path : "ordered_products",
        page : "ordered_products_reference_list",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            sHTML = "";
            sHTML += "<label for='reference-choice'>Rechercher par référence : </label>";
            sHTML += "<br/>";
            sHTML += "<input list='references' id='reference-choice' name='reference-choice' onKeyUp='CompareReference()'/>";
            sHTML += "<datalist id='references'>";
            var iReference = 0;
            for (var ligne in result)	{
                aOfReferences[iReference] = [];
                aOfReferences[iReference]["id_product"] = result[ligne]["id_product"];
                aOfReferences[iReference]["reference"] = htmlspecialchars_decode(result[ligne]["reference"]);

                sHTML += '<option value="' + aOfReferences[iReference]["reference"] + '"></option>';

                iReference++;
            }
            sHTML += "</datalist>";
            sHTML += "<span class='input-group-btn'><button id='btn_load_orders_by_reference' class='btn btn-success' style='display:none;'>Valider</button><span>";
            $('#datalist_reference').html(sHTML);
            $('#divModalLoading').hide();
        })
        .fail(function(err) {
            // error
            alert('error : ' + err.status);
        });
}

function CompareReference() {
    var selectedReference = document.getElementById("reference-choice").value;
    var btnLoadOrdersByReference = document.getElementById("btn_load_orders_by_reference");
    var btnVisible = false;

    for (var i = 0 ; i < aOfReferences.length; i++)
    {
        if (selectedReference == aOfReferences[i]["reference"])
        {
            buttonVisible=false;
            btnLoadOrdersByReference.setAttribute("onclick", "loadOrdersByReference(" + aOfReferences[i]["id_product"] + ");");
            btnLoadOrdersByReference.style.display = "inline-block";
            break;
        }
        else
        {
            buttonVisible = true;
        }

        if (buttonVisible == true)
        {
            btnLoadOrdersByReference.style.display = "none";
        }
    }
}

function loadOrdersByReference(idProduct) {
    aOfOrders = [];
    var div_image = document.getElementById("product_image");
    var image;

    $('#divModalLoading').show();
    var datas = {
        path : "ordered_products",
        page : "ordered_products_orders_by_reference_list",
        bJSON : 1,
        id_product: idProduct
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            document.getElementById("reference-choice").value = "";

            var iOrder = 0;
            for (var ligne in result)	{
                aOfOrders[iOrder] = [];
                aOfOrders[iOrder]["id_order"] = result[ligne]["id_order"];
                aOfOrders[iOrder]["id_product"] = result[ligne]["id_product"];
                aOfOrders[iOrder]["firstname"] = htmlspecialchars_decode(result[ligne]["firstname"]);
                aOfOrders[iOrder]["lastname"] = htmlspecialchars_decode(result[ligne]["lastname"]);
                aOfOrders[iOrder]["invoice_date"] = htmlspecialchars_decode(result[ligne]["invoice_date"]);
                aOfOrders[iOrder]["email"] = htmlspecialchars_decode(result[ligne]["email"]);
                aOfOrders[iOrder]["phone"] = htmlspecialchars_decode(result[ligne]["phone"]);
                aOfOrders[iOrder]["phone_mobile"] = htmlspecialchars_decode(result[ligne]["phone_mobile"]);
                aOfOrders[iOrder]["product_quantity"] = htmlspecialchars_decode(result[ligne]["product_quantity"]);
                aOfOrders[iOrder]["current_state"] = htmlspecialchars_decode(result[ligne]["current_state"]);
                aOfOrders[iOrder]["id_image"] = result[ligne]["id_image"];
                aOfOrders[iOrder]["link_rewrite"] = result[ligne]["link_rewrite"];
                image = 'https://www.pincab.eu/' + aOfOrders[iOrder]["id_image"] + '-small_default/' + aOfOrders[iOrder]["link_rewrite"] + '.jpg';

                iOrder++;
            }
            var elem = document.createElement("img");
            elem.setAttribute("src", image);
            elem.setAttribute("height", "92");
            elem.setAttribute("width", "92");
            div_image.appendChild(elem);

            // INIT DATATABLE
            if (submitted == 1) {
                div_image.removeChild(div_image.childNodes[0]);
                rebuildDatatableOrders();
                $('#divModalLoading').hide();
            }
            else if (submitted == 0) {
                constructTableOrders();
                // reload datatable configuration
                tablesOrders = $('#datatable').DataTable(configuration);
                submitted = 1;
                $('#divModalLoading').hide();
            }
        })
        .fail(function(err) {
            // error
            alert('error : ' + err.status);
        });
}

// construit la table avec les données
function constructTableOrders() {
    var i;
    // tableau datatable
    var sHTML = '<table id="datatable" class="table table-striped table-bordered" style="width:100%">';

    sHTML += "<thead>";
    sHTML += "<tr>";
    sHTML += "<th>ID commande</th>";
    sHTML += "<th>ID produit</th>";
    sHTML += "<th>Prénom</th>";
    sHTML += "<th>Nom</th>";
    sHTML += "<th>Mail</th>";
    sHTML += "<th>Date hidden</th>";
    sHTML += "<th>Date</th>";
    sHTML += "<th>Téléphone</th>";
    sHTML += "<th>Quantité commandée</th>";
    sHTML += "</tr>";
    sHTML += "</thead>";

    sHTML += "<tbody>";

    // pour chaque ligne de résultat, construire la ligne dans datatable
    for(i = 0; i < aOfOrders.length; i++) {

        sHTML += '<tr style="text-align:center;">';

        sHTML += '<th scope="row" style="text-align: center;"><a href="https://www.pincab.eu/admin797xdo6aw/index.php?controller=AdminOrders&vieworder=&id_order=' + aOfOrders[i]["id_order"] + '&token=' + tokenAdminOrders + '" ' + 'target="_blank">' + aOfOrders[i]["id_order"] + '</a></th>';

        sHTML += '<th scope="row" style="text-align: center;"><a href="https://www.pincab.eu/admin797xdo6aw/index.php/sell/catalog/products/' + aOfOrders[i]["id_product"] + '?_token=' + tokenAdminProducts + '" ' + 'target="_blank">' + aOfOrders[i]["id_product"] + '</a></th>';

        sHTML += "<td>" + aOfOrders[i]["firstname"] + "</td>";

        sHTML += "<td>" + aOfOrders[i]["lastname"] + "</td>";

        sHTML += "<td>" + aOfOrders[i]["email"] + "</td>";

        sHTML += "<td>" + aOfOrders[i]["invoice_date"] + "</td>";

        var dateTime = new Date(aOfOrders[i]["invoice_date"]);
        dateTime = moment(dateTime).format("DD/MM/YYYY");
        sHTML += "<td>" + dateTime + "</td>";

        if (aOfOrders[i]["phone"] != "") {
            sHTML += "<td>" + aOfOrders[i]["phone"] + "</td>";
        }
        else if (aOfOrders[i]["phone_mobile"] != "") {
            sHTML += "<td>" + aOfOrders[i]["phone_mobile"] + "</td>";
        }


        if ((aOfOrders[i]["current_state"] == 4) || (aOfOrders[i]["current_state"] == 5)) {
            sHTML += '<td style="background-color: #26B99A">' + aOfOrders[i]["product_quantity"] + '</td>';
        }
        else {
            sHTML += '<td style="background-color: #CE5454">' + aOfOrders[i]["product_quantity"] + '</td>';
        }

        sHTML += "</tr>";
    }
    sHTML += "</tbody>";
    sHTML += "</table>";
    $('#order_client_table').html(sHTML);
}

/**
 * clear HTML table
 * clear and destroy datatable
 * build table and call database
 */
function rebuildDatatableOrders() {
    tablesOrders.clear();
    tablesOrders.destroy();
    constructTableOrders();
    tablesOrders = $('#datatable').DataTable(configuration);
}

// datatable configuration
// order : tri selon une colonne (0 = 1ère colonne) par ordre croissant (asc) ou décroissant (desc)
// pageLength : nombre de résultats affichés par défaut
// lenghtMenu : choix du nombre de résultats à afficher
// language : traduction
// columns :
// orderable : triable (true ou false)
// visible : colonne affichée ou non (ID par exemple, ou date (si on veut trier par la date par exemple)) (true ou false)
// searchable : filtrable par le champ de recherche
const configuration = {
    "stateSave": false,
    "order": [[5, "desc"]],
    "pageLength": 25,
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
    "language" : {
        "info": "Affichage des lignes _START_ &agrave; _END_ sur _TOTAL_ r&eacute;sultats au total",
        "emptyTable": "Aucun r&eacute;sultat disponible",
        "lengthMenu": "Affichage de _MENU_ r&eacute;sultats",
        "search" : "Rechercher : ",
        "zeroRecords" : "Aucun r&eacute;sultat trouv&eacute;",
        "paginate" : {
            "previous": "Pr&eacute;c&eacute;dent",
            "next": "Suivant"
        },
        "sInfoFiltered": "(filtr&eacutes &agrave; partir de _MAX_ r&eacute;sultats au total)",
        "sInfoEmpty": "Aucun r&eacute;sultat disponible"
    },
    "dom": 'flrtip',
    "columns": [
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "visible": false,
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
    ],
    "retrieve": true,
    "responsive": true,
    "autoWidth": false
};

/**
 * Init start
 *
 */
// une fois la page chargée, charger les références
$(document).ready(function() {
    loadTokenAdminOrders();
    loadTokenAdminProducts();
    loadReferences();
});