SELECT
DB_PREFIX_product.id_product,
DB_PREFIX_order_detail.id_order,
DB_PREFIX_customer.firstname,
DB_PREFIX_customer.lastname,
DB_PREFIX_orders.invoice_date,
DB_PREFIX_customer.email,
CASE
    WHEN DB_PREFIX_address.phone = "" OR DB_PREFIX_address.phone IS NULL
        THEN DB_PREFIX_address.phone_mobile
    ELSE DB_PREFIX_address.phone
END AS phone,
DB_PREFIX_order_detail.product_quantity,
DB_PREFIX_orders.current_state,
DB_PREFIX_image_shop.id_image,
DB_PREFIX_product_lang.link_rewrite

FROM DB_PREFIX_product
INNER JOIN DB_PREFIX_order_detail ON DB_PREFIX_product.id_product = DB_PREFIX_order_detail.product_id
LEFT JOIN DB_PREFIX_orders ON DB_PREFIX_order_detail.id_order = DB_PREFIX_orders.id_order
LEFT JOIN DB_PREFIX_customer ON DB_PREFIX_orders.id_customer = DB_PREFIX_customer.id_customer
LEFT JOIN DB_PREFIX_address ON DB_PREFIX_customer.id_customer = DB_PREFIX_address.id_customer
LEFT JOIN DB_PREFIX_image_shop ON DB_PREFIX_product.id_product = DB_PREFIX_image_shop.id_product AND DB_PREFIX_image_shop.cover = 1
LEFT JOIN DB_PREFIX_product_lang ON DB_PREFIX_product.id_product = DB_PREFIX_product_lang.id_product
WHERE DB_PREFIX_product.id_product = '@id_product' AND DB_PREFIX_orders.current_state <> 6
GROUP BY DB_PREFIX_order_detail.id_order, DB_PREFIX_product.id_product
ORDER BY DB_PREFIX_order_detail.id_order;