SELECT DB_PREFIX_product.id_product, DB_PREFIX_product.reference
FROM DB_PREFIX_product
INNER JOIN DB_PREFIX_order_detail ON DB_PREFIX_product.id_product = DB_PREFIX_order_detail.product_id
GROUP BY DB_PREFIX_product.id_product;