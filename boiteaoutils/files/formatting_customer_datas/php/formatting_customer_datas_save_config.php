<?php
require_once "formatting_customer_datas_service.php";

/**
 * Class Update_film | file Update_film.php
 *
 * In this class, we show the interface "Update_film.html".
 * With this interface, we'll be able to update a movie with its id
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Formatting_customer_datas_save_config	{

    /**
     * public $resultat is used to store all datas needed for HTML Templates
     * @var array
     */
    public $resultat;

    /**
     * init variables resultat
     *
     * execute main function
     */
    public function __construct()	{
        // init variables resultat
        $this->resultat = [];

        // execute main function
        $this->main();
    }

    /**
     * Update a movie with its id
     */
    function main()	{
        $objet_formatting_customer_datas_save_config = new Formatting_customer_datas_service();

        if (isset($objet_formatting_customer_datas_save_config->VARS_HTML["last_name"])) {
            $file = $_SERVER['DOCUMENT_ROOT']. "/modules/boiteaoutils/files/formatting_customer_datas/formatting_customer_datas_config.txt";
            $config = fopen($file, "w") or die("Unable to open file!");

            $write = "0 : Ne pas formater ; 1 : Tout en majuscules ; 2 : 1ere lettre en majuscule" . "\n" . "\n";

            $write .= "last_name" . "=" . $objet_formatting_customer_datas_save_config->VARS_HTML["last_name"] . "\n";
            $write .= "first_name" . "=" . $objet_formatting_customer_datas_save_config->VARS_HTML["first_name"] . "\n";
            $write .= "company" . "=" . $objet_formatting_customer_datas_save_config->VARS_HTML["company"] . "\n";
            $write .= "address" . "=" . $objet_formatting_customer_datas_save_config->VARS_HTML["address"] . "\n";
            $write .= "city" . "=" . $objet_formatting_customer_datas_save_config->VARS_HTML["city"] . "\n";

            fwrite($config, $write);
            fclose($config);
        }

        $this->resultat = $objet_formatting_customer_datas_save_config->resultat;
        $this->VARS_HTML = $objet_formatting_customer_datas_save_config->VARS_HTML;
    }
}
?>
