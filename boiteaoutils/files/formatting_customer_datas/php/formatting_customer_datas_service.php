<?php
require_once __DIR__ . "/../../includes/php/initialize.php";

/**
 * Class film_service | file film_service.php
 *
 * In this class, we have methods for :
 * - adding a movie with method save_film()
 * - updating a movie with method update_film()
 * - deleting a movie with method supprime_film()
 * - listing all movies with method liste_film()
 * - editing a movie with method edit_film()
 * With this interface, we'll be able to list all the films stored in database
 *
 * List of classes needed for this class
 *
 * require_once "film_service.php";
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Formatting_customer_datas_service extends Initialize	{

    /**
     * public $resultat is used to store all datas needed for HTML Templates
     * @var array
     */
    public $resultat;

    /**
     * Call the parent constructor
     *
     * init variables resultat
     */
    public function __construct()	{
        // Call Parent Constructor
        parent::__construct();

        // init variables resultat
        $this->resultat= [];
    }

    /**
     * Call the parent destructor
     */
    public function __destruct()	{
        // Call Parent destructor
        parent::__destruct();
    }

    public function formatting_customer_datas_load_config() {

        $included_files = get_included_files();
        $path = "";
        $path_parts = explode("/", $included_files[0]);
        $i = 0;

        while (($path_parts[$i] != 'modules') && ($i < count($path_parts))) {
            $path .= $path_parts[$i] . "/";
            $i++;
        }

        $lines = file($path . "modules/boiteaoutils/files/formatting_customer_datas/formatting_customer_datas_config.txt");

        for ($i = 0; $i < count($lines); $i++) {
            if (preg_match("/\blast_name\b/i", $lines[$i], $match)) {
                $last_name_explode = explode('=',$lines[$i]);
                $last_name_option = rtrim($last_name_explode[1]);
            }
            if (preg_match("/\bfirst_name\b/i", $lines[$i], $match)) {
                $first_name_explode = explode('=',$lines[$i]);
                $first_name_option = rtrim($first_name_explode[1]);
            }
            if (preg_match("/\bcompany\b/i", $lines[$i], $match)) {
                $company_explode = explode('=',$lines[$i]);
                $company_option = rtrim($company_explode[1]);
            }
            if (preg_match("/\baddress\b/i", $lines[$i], $match)) {
                $address_explode = explode('=',$lines[$i]);
                $address_option = rtrim($address_explode[1]);
            }
            if (preg_match("/\bcity\b/i", $lines[$i], $match)) {
                $city_explode = explode('=',$lines[$i]);
                $city_option = rtrim($city_explode[1]);
            }
        }
        $formatting_customer_datas_config = [];
        $formatting_customer_datas_config['last_name'] = $last_name_option;
        $formatting_customer_datas_config['first_name'] = $first_name_option;
        $formatting_customer_datas_config['company'] = $company_option;
        $formatting_customer_datas_config['address'] = $address_option;
        $formatting_customer_datas_config['city'] = $city_option;


        $this->resultat["formatting_customer_datas_load_config"] = $formatting_customer_datas_config;
    }


    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_last_name_all_upper_address()    {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_last_name_all_upper.sql";
        $this->resultat["formatting_customer_datas_format_last_name_all_upper_address"] = $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_last_name_all_upper_customer()   {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_customer_last_name_all_upper.sql";
        $this->resultat["formatting_customer_datas_format_last_name_all_upper_customer"]= $this->oBdd->treatDatas($spathSQL, array());
    }


    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_last_name_first_upper_address()    {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_last_name_first_upper.sql";
        $this->resultat["formatting_customer_datas_format_last_name_first_upper_address"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_last_name_first_upper_customer()   {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_customer_last_name_first_upper.sql";
        $this->resultat["formatting_customer_datas_format_last_name_first_upper_customer"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_first_name_all_upper_address()   {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_first_name_all_upper.sql";
        $this->resultat["formatting_customer_datas_format_first_name_all_upper_address"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_first_name_all_upper_customer()  {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_customer_first_name_all_upper.sql";
        $this->resultat["formatting_customer_datas_format_first_name_all_upper_customer"]= $this->oBdd->treatDatas($spathSQL, array());
    }


    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_first_name_first_upper_address()   {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_first_name_first_upper.sql";
        $this->resultat["formatting_customer_datas_format_first_name_first_upper_address"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_first_name_first_upper_customer()  {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_customer_first_name_first_upper.sql";
        $this->resultat["formatting_customer_datas_format_first_name_first_upper_customer"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_company_all_upper()  {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_company_all_upper.sql";
        $this->resultat["formatting_customer_datas_format_company_all_upper"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_company_first_upper()  {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_company_first_upper.sql";
        $this->resultat["formatting_customer_datas_format_company_first_upper"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_adress_1_all_upper()    {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_address_1_all_upper.sql";
        $this->resultat["formatting_customer_datas_format_adress_1_all_upper"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function Formatting_customer_datas_format_adress_1_first_upper()    {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_address_1_first_upper.sql";
        $this->resultat["formatting_customer_datas_format_adress_1_first_upper"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_adress_2_all_upper()    {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_address_2_all_upper.sql";
        $this->resultat["formatting_customer_datas_format_adress_2_all_upper"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_adress_2_first_upper()    {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_address_2_first_upper.sql";
        $this->resultat["formatting_customer_datas_format_adress_2_first_upper"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_city_all_upper() {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_city_all_upper.sql";
        $this->resultat["formatting_customer_datas_format_city_all_upper"]= $this->oBdd->treatDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function formatting_customer_datas_format_city_first_upper() {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'formatting_customer_datas/' . 'sql/' . "formatting_customer_datas_UPDATE_address_city_first_upper.sql";
        $this->resultat["formatting_customer_datas_format_city_first_upper"]= $this->oBdd->treatDatas($spathSQL, array());
    }
}

?>
