<?php
require_once "delivery_tracking_service.php";

/**
 * Class Update_film | file Update_film.php
 *
 * In this class, we show the interface "Update_film.html".
 * With this interface, we'll be able to update a movie with its id
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Delivery_tracking_update_current_state	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	 * Update a movie with its id
	 */
	function main()	{
		$objet_delivery_tracking_update_current_state = new Delivery_tracking_service();
		$objet_delivery_tracking_update_current_state->delivery_tracking_update_orders_current_state();
		$objet_delivery_tracking_update_current_state->delivery_tracking_update_order_history_current_state();

		$this->resultat = $objet_delivery_tracking_update_current_state->resultat;
		$this->VARS_HTML = $objet_delivery_tracking_update_current_state->VARS_HTML;
	}
}
?>
