<?php

$included_files = get_included_files();
$path_parts = explode("/", $included_files[0]);
$i = 0;

while (($path_parts[$i] != 'modules') && ($i < count($path_parts))) {
    $path .= $path_parts[$i] . "/";
    $i++;
}

$DB_infos = require $path . 'app/config/parameters.php';

$servername = $DB_infos["parameters"]["database_host"];
$username = $DB_infos["parameters"]["database_user"];
$password = $DB_infos["parameters"]["database_password"];
$dbname = $DB_infos["parameters"]["database_name"];

$conn = new mysqli($servername, $username, $password,$dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$message = "";

$sql = "
UPDATE `PB_mailalert_customer_oos` 
INNER JOIN `PB_customer` ON `PB_mailalert_customer_oos`.`customer_email` = `PB_customer`.`email`
SET `PB_mailalert_customer_oos`.id_customer =`PB_customer`.id_customer
WHERE `PB_mailalert_customer_oos`.id_customer = 0;";



$result = mysqli_query($conn,"SELECT `value` FROM `PB_configuration` WHERE `name` = 'PS_SHOP_EMAIL';");
while ($row = $result->fetch_assoc())
{
    $mail = $row["value"];
}

$mail = "julien.ducharlet@gmail.com";

if ($conn->query($sql) === TRUE) {
    $message .= "Les demandes d'alertes ont bien été requalifiées";
    mail($mail, 'CRON customer mail alert stock', $message);
}
else {
    $message .= "Une erreur s'est produite";
    mail($mail, 'CRON customer mail alert stock', $message);
}