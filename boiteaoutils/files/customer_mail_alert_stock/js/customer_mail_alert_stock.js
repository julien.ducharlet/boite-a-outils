var documentRoot;

function loadDocumentRoot() {
    var datas = {
        path : "utils",
        page : "get_document_root",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            documentRoot = result;
            var cron_command_div = document.getElementById("cron_command");
            cron_command_div.innerHTML = "Pour lancer le cron, utilisez la commande suivante : /usr/local/bin/php " + documentRoot + "/modules/boiteaoutils/files/customer_mail_alert_stock/cron/cron_customer_mail_alert_stock.php";
        })
        .fail(function(err) {
            console.log("error");
            // error
        });
}

/**
 * public aOfFilms is used to store all datas of movies
 * @var array
 */
var aOfCustomers = [];

// variable contenant la datatable
var tablesCustomers;


/**
 * Get Movies from database
 *
 * if OK add movies to array aOfFilms
 *
 * if OK then build table and call datatable
 */
// charge la liste des stocks en Ajax et construit le tableau JS
function loadCustomers() {
    $('#divModalLoading').show();
    var datas = {
        path : "customer_mail_alert_stock",
        page : "customer_mail_alert_stock_list",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            var iCustomer = 0;
            aOfCustomers = [];

            for (var ligne in result)	{
                aOfCustomers[iCustomer] = [];
                aOfCustomers[iCustomer]["id_customer"] = result[ligne]["id_customer"];
                aOfCustomers[iCustomer]["email"] = result[ligne]["email"];
				aOfCustomers[iCustomer]["id_product"] = result[ligne]["id_product"];
                aOfCustomers[iCustomer]["name"] = result[ligne]["name"];
				aOfCustomers[iCustomer]["lastname"] = result[ligne]["lastname"];
                aOfCustomers[iCustomer]["firstname"] = result[ligne]["firstname"];
                aOfCustomers[iCustomer]["id_product_attribute"] = result[ligne]["id_product_attribute"];
                aOfCustomers[iCustomer]["product_name"] = result[ligne]["product_name"];

                iCustomer++;
            }

            // INIT DATATABLE
            constructTableCustomers();
            // reload datatable configuration
            tablesCustomers = $('#datatable').DataTable(configuration);
            $('#divModalLoading').hide();

            /*var datatable_filter = $('#datatable_filter').parent();
            var datatable_length = $('#datatable_length').parent();

            var datatable_filter_clone = datatable_filter.clone();
            var datatable_length_clone = datatable_length.clone();

            datatable_filter.replaceWith(datatable_length_clone);
            datatable_length.replaceWith(datatable_filter_clone);

            //$('#datatable_filter').css({'float':'left'});
            //$('#datatable_length').css({'float':'right'});*/

        })
        .fail(function(err) {
            // error
            alert('error : ' + err.status);
        });
}


function updateCustomers() {
    var div_update_customers_alert_success = document.getElementById("update_customers_alert_success");
    var div_update_customers_alert_failed = document.getElementById("update_customers_alert_failed");
    var datas = {
        path : "customer_mail_alert_stock",
        page: "customer_mail_alert_stock_update",
        bJSON: 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function (result) {
            console.log(result);
            div_update_customers_alert_success.innerHTML += "<strong>Les demandes d'alertes ont bien été requalifiées</strong><br/>";
            div_update_customers_alert_success.style.display = "block";
        })
        .fail(function (err) {
            alert('error : ' + err.status);
        });
}

function deleteCustomers() {
    for(i = 0; i < aOfCustomers.length; i++) {
        if ($('#checkbox_' + i).is(":checked")) {
            //console.log("checked " + i);
            //console.log(aOfCustomers[i]["id_product"]);
            var datas = {
                path : "customer_mail_alert_stock",
                page: "customer_mail_alert_stock_delete",
                bJSON: 1,
                'email': aOfCustomers[i]["email"],
                'id_product': aOfCustomers[i]["id_product"],
                'id_product_attribute': aOfCustomers[i]["id_product_attribute"],
            }
            $.ajax({
                type: "POST",
                url: "route.php",
                async: true,
                data: datas,
                dataType: "json",
                cache: false
            })
                .done(function (result) {
                    console.log(result);
                    console.log("deleted");
                    loadCustomers();
                })
                .fail(function (err) {
                    alert('error : ' + err.status);
                });
        }
        else {
            //console.log("not checked " + i);
        }
    }
}

function bulkAction() {
    //alert($("#bulk_option_select").val());
    if ($("#bulk_option_select").val() === 'delete_checked') {
        deleteCustomers();
    }
    else if ($("#bulk_option_select").val() === 'export_checked') {
        alert("WIP export");
    }
}


function selectAllCustomers() {
    if ($('#bulk_action_select_all').not(":checked")) {
        for(i = 0; i < aOfCustomers.length; i++) {
            if ($('#checkbox_' + i).is(":checked")) {
                console.log("checked " + i);
                $('#checkbox_' + i).prop('checked', false);
            }
        }
    }
    if ($('#bulk_action_select_all').is(":checked")) {
        for(i = 0; i < aOfCustomers.length; i++) {
            if ($('#checkbox_' + i).not(":checked")) {
                console.log("not checked " + i);
                $('#checkbox_' + i).prop('checked', true);
            }
        }
    }
}

// construit la table avec les données
function constructTableCustomers() {
    var i;
    // tableau datatable
    var sHTML = "";

    sHTML += '<table id="datatable" class="table table-striped table-bordered" style="width:100%">';
    sHTML += "<thead>";
    sHTML += "<tr>";
    sHTML += '<th style="text-align:center;"><input type="checkbox" id="bulk_action_select_all" onclick="selectAllCustomers()"></th>';
    sHTML += "<th>ID CLIENT </th>";
	sHTML += "<th style='padding-right: 15px !important;'>EMAIL CLIENT</th>";
	sHTML += "<th>ID PRODUIT</th>";
	sHTML += "<th>NOM PRODUIT</th>";
	sHTML += "<th>PRENOM </th>";
	sHTML += "<th>NOM</th>";
	sHTML += "<th>OPTION PRODUIT</th>";
	sHTML += "<th>OPTION NOM PRODUIT</th>";

    sHTML += "</tr>";
    sHTML += "</thead>";
    sHTML += "<tbody>";

    // pour chaque ligne de résultat, construire la ligne dans datatable
    for(i = 0; i < aOfCustomers.length; i++) {

        sHTML += '<tr style="text-align:center;">';

        sHTML += '<td>' + '<input type="checkbox" id="checkbox_' + i + '">'  + '</td>';

        sHTML += "<td>" + aOfCustomers[i]["id_customer"] + "</td>";

		sHTML += "<td>" + aOfCustomers[i]["email"] + "</td>";
		
		sHTML += "<td>" + aOfCustomers[i]["id_product"] + "</td>";
				
		sHTML += "<td>" + aOfCustomers[i]["name"] + "</td>";
		
		sHTML += "<td>" + aOfCustomers[i]["firstname"] + "</td>";
		
		sHTML += "<td>" + aOfCustomers[i]["lastname"] + "</td>";
		
		sHTML += "<td>" + aOfCustomers[i]["id_product_attribute"] + "</td>";

		sHTML += "<td>" + aOfCustomers[i]["product_name"] + "</td>";
    }
    sHTML += "</tbody>";
    sHTML += "</table>";
    $('#customer_mail_alert_stock_table').html(sHTML);
}

/**
 * clear HTML table
 * clear and destroy datatable
 * build table and call database
 */
function rebuildDatatableStocks() {
    tablesCustomers.clear();
    tablesCustomers.destroy();
    constructTableCustomers();
    tablesCustomers = $('#datatable').DataTable(configuration);
}

// datatable configuration
// order : tri selon une colonne (0 = 1ère colonne) par ordre croissant (asc) ou décroissant (desc)
// pageLength : nombre de résultats affichés par défaut
// lenghtMenu : choix du nombre de résultats à afficher
// language : traduction
// columns :
    // orderable : triable (true ou false)
    // visible : colonne affichée ou non (ID par exemple, ou date (si on veut trier par la date par exemple)) (true ou false)
    // searchable : filtrable par le champ de recherche
const configuration = {
    "stateSave": false,
    "order": [[3, "asc"], [1, "asc"]],
    "pageLength": 25,
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthMenu": [[10, 25, 50, 100, 250, -1], [10, 25, 50, 100, 250, "Tous"]],
    "language" : {
        "info": "Affichage des lignes _START_ &agrave; _END_ sur _TOTAL_ r&eacute;sultats au total",
        "emptyTable": "Aucun r&eacute;sultat disponible",
        "lengthMenu": "Affichage de _MENU_ r&eacute;sultats",
        "search" : "Rechercher : ",
        "zeroRecords" : "Aucun r&eacute;sultat trouv&eacute;",
        "paginate" : {
            "previous": "Pr&eacute;c&eacute;dent",
            "next": "Suivant"
        },
        "sInfoFiltered": "(filtr&eacutes &agrave; partir de _MAX_ r&eacute;sultats au total)",
        "sInfoEmpty": "Aucun r&eacute;sultat disponible"
    },
    "dom": 'flrtip',
    "columns": [
        {
            "orderable": false,
        },
        {
            "orderable": true,
            "width": "5%"
        },
		 {
            "orderable": true,
             "width": "5%"
        },
		 {
            "orderable": true,
             "width": "5%"
        },
		{
            "orderable": true,
            "width": "15%"
        },
		{
            "orderable": true,
            "width": "10%"
        },
		{
            "orderable": true,
            "width": "10%"
        },
		{
            "visible": false
        },
        {
            "orderable": true
        }
    ],
    "retrieve": true,
    "responsive": true,
    "autoWidth": false
};

/**
 * Init start
 *
 */
// une fois la page chargée, charger les stocks
$(document).ready(function() {
    loadCustomers();
    loadDocumentRoot();
});