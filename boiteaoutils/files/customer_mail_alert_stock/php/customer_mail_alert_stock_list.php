<?php
require_once "customer_mail_alert_stock_service.php";

/**
 * Class Liste_film | file liste_film.php
 *
 * In this class, we show the interface "liste_film.html".
 * With this interface, we'll be able to list all the films stored in database
 *
 * List of classes needed for this class
 *
 * require_once "film_service.php";
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Customer_mail_alert_stock_list	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat = [];

		// execute main function
		$this->main();
	}

	/**
	 * Get list of all movies
	 */
	function main()	{
		// List 'em all !!
		$customer_mail_alert_stock_list = new Customer_mail_alert_stock_service();
		$customer_mail_alert_stock_list->customer_mail_alert_stock_list();
		
		// Get elements for the view
		$this->resultat = $customer_mail_alert_stock_list->resultat;
		$this->VARS_HTML = $customer_mail_alert_stock_list->VARS_HTML;
		
		// kill object
		unset($customer_mail_alert_stock_list);
	}
}

?>