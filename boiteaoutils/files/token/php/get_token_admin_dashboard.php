<?php
require_once "token_service.php";

/**
 * Class Liste_film | file liste_film.php
 *
 * In this class, we show the interface "liste_film.html".
 * With this interface, we'll be able to list all the films stored in database
 *
 * List of classes needed for this class
 *
 * require_once "film_service.php";
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Get_token_admin_dashboard	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	 * Get list of all movies
	 */
	function main()	{
		// List 'em all !!
		$obj_get_token_admin_dashboard = new Token_service();
        $obj_get_token_admin_dashboard->get_token_admin_dashboard();
		
		// Get elements for the view
		$this->resultat = $obj_get_token_admin_dashboard->resultat;
		$this->VARS_HTML = $obj_get_token_admin_dashboard->VARS_HTML;
		
		// kill object
		unset($obj_get_token_admin_dashboard);
	}
}

?>