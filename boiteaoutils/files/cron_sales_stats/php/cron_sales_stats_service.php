<?php
require_once __DIR__ . "/../../includes/php/initialize.php";

/**
 * Class film_service | file film_service.php
 *
 * In this class, we have methods for :
 * - adding a movie with method save_film()
 * - updating a movie with method update_film()
 * - deleting a movie with method supprime_film()
 * - listing all movies with method liste_film()
 * - editing a movie with method edit_film()
 * With this interface, we'll be able to list all the films stored in database
 *
 * List of classes needed for this class
 *
 * require_once "film_service.php";
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Cron_sales_stats_service extends Initialize	{

    /**
     * public $resultat is used to store all datas needed for HTML Templates
     * @var array
     */
    public $resultat;

    /**
     * Call the parent constructor
     *
     * init variables resultat
     */
    public function __construct()	{
        // Call Parent Constructor
        parent::__construct();

        // init variables resultat
        $this->resultat = [];
    }

    /**
     * Call the parent destructor
     */
    public function __destruct()	{
        // Call Parent destructor
        parent::__destruct();
    }

    /**
     * Method liste_film()
     *
     * List all movies in database
     */
    public function cron_sales_stats_products_and_attributes_list()	{
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'cron_sales_stats/' . 'sql/' . "cron_sales_stats_SELECT_product_product_and_attribute.sql";
        $this->resultat["cron_sales_stats_products_and_attributes_list"] = $this->oBdd->getSelectDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function cron_sales_stats_update_date_add() {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'cron_sales_stats/' . 'sql/' . "cron_sales_stats_UPDATE_bao_product_sales_stats_date_add.sql";
        $this->resultat["cron_sales_stats_update_date_add"] = $this->oBdd->treatDatas($spathSQL, array(
            "product_id" => $this->VARS_HTML["product_id"],
            "product_attribute_id" => $this->VARS_HTML["product_attribute_id"],
            "date_add" => $this->VARS_HTML["date_add"]
        ));
    }

    /**
     * Method liste_film()
     *
     * List all movies in database
     */
    public function cron_sales_stats_duration_all_list()	{
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'cron_sales_stats/' . 'sql/' . "cron_sales_stats_SELECT_order_detail_sales_stats_duration_all.sql";
        $this->resultat["cron_sales_stats_duration_all_list"] = $this->oBdd->getSelectDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function cron_sales_stats_update_duration_all()  {
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'cron_sales_stats/' . 'sql/' . "cron_sales_stats_UPDATE_bao_product_sales_stats_duration_all.sql";
        $this->resultat["cron_sales_stats_update_duration_all"] = $this->oBdd->treatDatas($spathSQL, array(
            "product_id" => $this->VARS_HTML["product_id"],
            "product_attribute_id" => $this->VARS_HTML["product_attribute_id"],
            "sales_stats_duration_all" => $this->VARS_HTML["sales_stats_duration_all"]
        ));
    }

    /**
     * Method liste_film()
     *
     * List all movies in database
     */
    public function cron_sales_stats_duration_one_list()	{
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'cron_sales_stats/' . 'sql/' . "cron_sales_stats_SELECT_order_detail_sales_stats_duration_one.sql";
        $this->resultat["cron_sales_stats_duration_one_list"] = $this->oBdd->getSelectDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function cron_sales_stats_update_duration_one(){
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'cron_sales_stats/' . 'sql/' . "cron_sales_stats_UPDATE_bao_product_sales_stats_duration_one.sql";
        $this->resultat["cron_sales_stats_update_duration_one"] = $this->oBdd->treatDatas($spathSQL, array(
            "product_id" => $this->VARS_HTML["product_id"],
            "product_attribute_id" => $this->VARS_HTML["product_attribute_id"],
            "sales_stats_duration_1" => $this->VARS_HTML["sales_stats_duration_1"]
        ));
    }

    /**
     * Method liste_film()
     *
     * List all movies in database
     */
    public function cron_sales_stats_duration_two_list()	{
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'cron_sales_stats/' . 'sql/' . "cron_sales_stats_SELECT_order_detail_sales_stats_duration_two.sql";
        $this->resultat["cron_sales_stats_duration_two_list"] = $this->oBdd->getSelectDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function cron_sales_stats_update_duration_two(){
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'cron_sales_stats/' . 'sql/' . "cron_sales_stats_UPDATE_bao_product_sales_stats_duration_two.sql";
        $this->resultat["cron_sales_stats_update_duration_two"] = $this->oBdd->treatDatas($spathSQL, array(
            "product_id" => $this->VARS_HTML["product_id"],
            "product_attribute_id" => $this->VARS_HTML["product_attribute_id"],
            "sales_stats_duration_2" => $this->VARS_HTML["sales_stats_duration_2"]
        ));
    }

    /**
     * Method liste_film()
     *
     * List all movies in database
     */
    public function cron_sales_stats_duration_three_list()	{
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'cron_sales_stats/' . 'sql/' . "cron_sales_stats_SELECT_order_detail_sales_stats_duration_three.sql";
        $this->resultat["cron_sales_stats_duration_three_list"] = $this->oBdd->getSelectDatas($spathSQL, array());
    }

    /**
     * Method update_film()
     *
     * Update the movie with param id_film in database
     */
    public function cron_sales_stats_update_duration_three(){
        // Here I can Access to :
        // $this->GLOBALS_INI
        // $this->oBdd
        // $this->VARS_HTML
        $spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'cron_sales_stats/' . 'sql/' . "cron_sales_stats_UPDATE_bao_product_sales_stats_duration_three.sql";
        $this->resultat["cron_sales_stats_update_duration_three"] = $this->oBdd->treatDatas($spathSQL, array(
            "product_id" => $this->VARS_HTML["product_id"],
            "product_attribute_id" => $this->VARS_HTML["product_attribute_id"],
            "sales_stats_duration_3" => $this->VARS_HTML["sales_stats_duration_3"]
        ));
    }
}

?>
