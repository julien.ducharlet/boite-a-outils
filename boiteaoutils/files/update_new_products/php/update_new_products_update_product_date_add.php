<?php
require_once "update_new_products_service.php";

/**
 * Class Update_film | file Update_film.php
 *
 * In this class, we show the interface "Update_film.html".
 * With this interface, we'll be able to update a movie with its id
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Update_new_products_update_product_date_add	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat = [];

		// execute main function
		$this->main();
	}

	/**
	 * Update a movie with its id
	 */
	function main()	{
		$objet_update_new_products_update_product_date_add = new Update_new_products_service();
		$objet_update_new_products_update_product_date_add->update_new_products_update_product_date_add();

		$this->resultat = $objet_update_new_products_update_product_date_add->resultat;
		$this->VARS_HTML = $objet_update_new_products_update_product_date_add->VARS_HTML;
	}
}
?>
