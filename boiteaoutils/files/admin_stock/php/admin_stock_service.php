<?php
require_once __DIR__ . "/../../includes/php/initialize.php";

/**
 * Class film_service | file film_service.php
 *
 * In this class, we have methods for :
 * - adding a movie with method save_film()
 * - updating a movie with method update_film()
 * - deleting a movie with method supprime_film()
 * - listing all movies with method liste_film()
 * - editing a movie with method edit_film()
 * With this interface, we'll be able to list all the films stored in database
 *
 * List of classes needed for this class
 *
 * require_once "film_service.php";
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Admin_stock_service extends Initialize
{

	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * Call the parent constructor
	 *
	 * init variables resultat
	 */
	public function __construct()
	{
		// Call Parent Constructor
		parent::__construct();

		// init variables resultat
		$this->resultat = [];
	}

	/**
	 * Call the parent destructor
	 */
	public function __destruct()
	{
		// Call Parent destructor
		parent::__destruct();
	}

	/**
	 * Method liste_film()
	 *
	 * List all movies in database
	 */
	public function admin_stock_list()
	{
		$spathSQL = $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'admin_stock/' . 'sql/' . "admin_stock_SELECT_all.sql";
		$this->resultat["admin_stock_list"] = $this->oBdd->getSelectDatas($spathSQL, array());
	}

	public function admin_stock_update_bao_product_ideal_stock()
	{
		$spathSQL = $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'admin_stock/' . 'sql/' . "admin_stock_UPDATE_bao_product_ideal_stock.sql";
		$this->resultat["admin_stock_update_bao_product_ideal_stock"] = $this->oBdd->treatDatas($spathSQL, array(
			"product_id" => $this->VARS_HTML["product_id"],
			"product_attribute_id" => $this->VARS_HTML["product_attribute_id"],
			"ideal_stock" => $this->VARS_HTML["ideal_stock"]
		));
	}

	public function admin_stock_update_product_low_stock_threshold()
	{
		$spathSQL = $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'admin_stock/' . 'sql/' . "admin_stock_UPDATE_product_low_stock_threshold.sql";
		$this->resultat["admin_stock_update_product_low_stock_threshold"] = $this->oBdd->treatDatas($spathSQL, array(
			"id_product" => $this->VARS_HTML["id_product"],
			"low_stock_threshold" => $this->VARS_HTML["low_stock_threshold"]
		));
	}

	public function admin_stock_update_product_shop_low_stock_threshold()
	{
		$spathSQL = $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'admin_stock/' . 'sql/' . "admin_stock_UPDATE_product_shop_low_stock_threshold.sql";
		$this->resultat["admin_stock_update_product_shop_low_stock_threshold"] = $this->oBdd->treatDatas($spathSQL, array(
			"id_product" => $this->VARS_HTML["id_product"],
			"low_stock_threshold" => $this->VARS_HTML["low_stock_threshold"]
		));
	}

	public function admin_stock_update_product_attribute_low_stock_threshold()
	{
		$spathSQL = $this->GLOBALS_INI["PATH_HOME"] . 'files/'. 'admin_stock/' . 'sql/' . "admin_stock_UPDATE_product_attribute_low_stock_threshold.sql";
		$this->resultat["admin_stock_update_product_attribute_low_stock_threshold"] = $this->oBdd->treatDatas($spathSQL, array(
			"id_product" => $this->VARS_HTML["id_product"],
			"id_attribute" => $this->VARS_HTML["id_product_attribute"],
			"low_stock_threshold" => $this->VARS_HTML["low_stock_threshold"]
		));
	}
}
?>
