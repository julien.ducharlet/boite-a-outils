<?php
require_once "admin_stock_service.php";

/**
 * Class Update_film | file Update_film.php
 *
 * In this class, we show the interface "Update_film.html".
 * With this interface, we'll be able to update a movie with its id
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Admin_stock_update_low_stock_threshold	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	 * Update a movie with its id
	 */
	function main()	{
	    //update_product_low_stock_threshold
        //update_product_shop_low_stock_threshold
        //update_product_attribute_low_stock_threshold
        $objet_admin_stock_update_low_stock_threshold = new Admin_stock_service();
        if (isset($_POST["id_product_attribute"])) {
            if ($_POST["id_product_attribute"] == 0) {
                $objet_admin_stock_update_low_stock_threshold->admin_stock_update_product_low_stock_threshold();
                $objet_admin_stock_update_low_stock_threshold->admin_stock_update_product_shop_low_stock_threshold();
            }
            else {
                $objet_admin_stock_update_low_stock_threshold->admin_stock_update_product_attribute_low_stock_threshold();
            }
            $this->resultat= $objet_admin_stock_update_low_stock_threshold->resultat;
            $this->VARS_HTML= $objet_admin_stock_update_low_stock_threshold->VARS_HTML;
        }
	}
}
?>