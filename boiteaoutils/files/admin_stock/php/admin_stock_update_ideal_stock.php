<?php
require_once "admin_stock_service.php";

/**
 * Class Update_film | file Update_film.php
 *
 * In this class, we show the interface "Update_film.html".
 * With this interface, we'll be able to update a movie with its id
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Admin_stock_update_ideal_stock	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	 * Update a movie with its id
	 */
	function main()	{
		$objet_admin_stock_update_ideal_stock = new Admin_stock_service();
		$objet_admin_stock_update_ideal_stock->admin_stock_update_bao_product_ideal_stock();

		$this->resultat = $objet_admin_stock_update_ideal_stock->resultat;
		$this->VARS_HTML = $objet_admin_stock_update_ideal_stock->VARS_HTML;
	}
}
?>