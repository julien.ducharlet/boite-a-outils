INSERT INTO DB_PREFIX_bao_product_ideal_stock
(product_id, product_attribute_id, ideal_stock)
VALUES
   ('@product_id','@product_attribute_id', '@ideal_stock')
ON DUPLICATE KEY UPDATE
    ideal_stock = '@ideal_stock';