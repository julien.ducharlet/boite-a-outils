UPDATE DB_PREFIX_product_attribute
SET low_stock_threshold = '@low_stock_threshold'
WHERE id_product = '@id_product'
AND id_product_attribute = '@id_attribute';