SELECT
    PB_bao_product_sales_stats.product_id,
    PB_bao_product_sales_stats.product_attribute_id,
    PB_product.reference AS 'product_reference',
    COALESCE(PB_product_attribute_image.id_image, PB_image_shop.id_image) AS 'id_image',
    PB_product_lang.link_rewrite,
    PB_product_lang.name AS 'product_name',
    PB_attribute_group_lang.name AS 'product_attribute_group_name',
    PB_attribute_lang.name AS 'product_attribute_name',
    PB_bao_product_sales_stats.date_add,
    PB_bao_product_sales_stats.sales_stats_duration_1,
    PB_bao_product_sales_stats.sales_stats_duration_2,
    PB_bao_product_sales_stats.sales_stats_duration_3,
    PB_bao_product_sales_stats.sales_stats_duration_all,
    PB_stock_available.physical_quantity,
    PB_stock_available.reserved_quantity,
    PB_stock_available.quantity,
    CASE
        WHEN PB_bao_product_sales_stats.product_attribute_id = 0 THEN PB_product.low_stock_threshold
        ELSE PB_product_attribute.low_stock_threshold
    END AS low_stock_threshold,
    PB_bao_product_ideal_stock.ideal_stock,
    GROUP_CONCAT(PB_supplier.name) AS 'supplier_names',
    GROUP_CONCAT(PB_product_supplier.product_supplier_reference) AS 'supplier_references',
    GROUP_CONCAT(PB_product_supplier.product_supplier_price_te) AS 'supplier_prices_te',
    (PB_stock_available.quantity - ROUND((PB_bao_product_sales_stats.sales_stats_duration_1 + (PB_bao_product_sales_stats.sales_stats_duration_2/2) + (PB_bao_product_sales_stats.sales_stats_duration_3/6) + (PB_bao_product_sales_stats.sales_stats_duration_all/(DATEDIFF(DATE(NOW()),DATE(PB_product.date_add))/30.45)))/4)) AS 'etat_stock_dans_1_mois',
    (PB_bao_product_ideal_stock.ideal_stock - (PB_stock_available.quantity - (ROUND(((PB_bao_product_sales_stats.sales_stats_duration_1 + (PB_bao_product_sales_stats.sales_stats_duration_2/2) + (PB_bao_product_sales_stats.sales_stats_duration_3/6) + (PB_bao_product_sales_stats.sales_stats_duration_all / (DATEDIFF(DATE(NOW()),PB_product.date_add)/30.45)))/4) * (30.45/30.45))))) AS 'a_acheter_pour_avoir_bon_stock',
    ((ROUND(((PB_bao_product_sales_stats.sales_stats_duration_1 + (PB_bao_product_sales_stats.sales_stats_duration_2/2) + (PB_bao_product_sales_stats.sales_stats_duration_3/6))/3) * (30.45/30.45))) - PB_stock_available.quantity) AS 'a_acheter_qui_sera_vendu_dans_1_mois'
FROM PB_bao_product_sales_stats
LEFT JOIN PB_product ON PB_bao_product_sales_stats.product_id = PB_product.id_product
LEFT JOIN PB_image_shop ON PB_bao_product_sales_stats.product_id = PB_image_shop.id_product AND PB_image_shop.cover = 1
LEFT JOIN PB_product_attribute_image ON PB_bao_product_sales_stats.product_attribute_id = PB_product_attribute_image.id_product_attribute
LEFT JOIN PB_product_lang ON PB_bao_product_sales_stats.product_id = PB_product_lang.id_product
LEFT JOIN PB_product_attribute ON PB_bao_product_sales_stats.product_attribute_id = PB_product_attribute.id_product_attribute
LEFT JOIN PB_product_attribute_combination ON PB_bao_product_sales_stats.product_attribute_id = PB_product_attribute_combination.id_product_attribute
LEFT JOIN PB_attribute ON PB_product_attribute_combination.id_attribute = PB_attribute.id_attribute
LEFT JOIN PB_attribute_group_lang ON PB_attribute.id_attribute_group = PB_attribute_group_lang.id_attribute_group
LEFT JOIN PB_attribute_lang ON PB_product_attribute_combination.id_attribute = PB_attribute_lang.id_attribute
LEFT JOIN PB_stock_available ON PB_bao_product_sales_stats.product_id = PB_stock_available.id_product
LEFT JOIN PB_bao_product_ideal_stock ON PB_bao_product_sales_stats.product_id = PB_bao_product_ideal_stock.product_id AND PB_bao_product_sales_stats.product_attribute_id = PB_bao_product_ideal_stock.product_attribute_id
LEFT JOIN PB_product_supplier ON PB_bao_product_sales_stats.product_id = PB_product_supplier.id_product
LEFT JOIN PB_supplier ON PB_product_supplier.id_supplier = PB_supplier.id_supplier
GROUP BY PB_bao_product_sales_stats.product_id, PB_bao_product_sales_stats.product_attribute_id;