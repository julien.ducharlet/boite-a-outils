var tokenAdminProducts;

/**
 * public aOfFilms is used to store all datas of movies
 * @var array
 */
var aOfStocks = [];

// variable contenant la datatable
var tablesStocks;

function loadTokenAdminProducts() {
    var datas = {
        path : "token",
        page : "get_token_admin_products",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            var linkAdminProducts = result;
            var AdminProductsParts = linkAdminProducts.split('token=', 2);
            tokenAdminProducts = AdminProductsParts[1];
        })
        .fail(function(err) {
            console.log("error");
            // error
        });
}

/**
 * Get Movies from database
 *
 * if OK add movies to array aOfFilms
 *
 * if OK then build table and call datatable
 */
// charge la liste des stocks en Ajax et construit le tableau JS
function loadStocks() {
    $('#divModalLoading').show();
    var datas = {
        path : "manage_stock",
        page : "manage_stock_list",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            var iStock = 0;
            for (var ligne in result)	{
                aOfStocks[iStock] = [];
                aOfStocks[iStock]["product_id"] = result[ligne]["product_id"];
                aOfStocks[iStock]["product_reference"] = result[ligne]["product_reference"];
                aOfStocks[iStock]["id_image"] = result[ligne]["id_image"];
                aOfStocks[iStock]["link_rewrite"] = result[ligne]["link_rewrite"];
                aOfStocks[iStock]["product_name"] = result[ligne]["product_name"];
                aOfStocks[iStock]["product_attribute_group_name"] = result[ligne]["product_attribute_group_name"];
                aOfStocks[iStock]["product_attribute_name"] = result[ligne]["product_attribute_name"];
                aOfStocks[iStock]["date_add"] = result[ligne]["date_add"];
                aOfStocks[iStock]["sales_stats_duration_1"] = result[ligne]["sales_stats_duration_1"];
                aOfStocks[iStock]["sales_stats_duration_2"] = result[ligne]["sales_stats_duration_2"];
                aOfStocks[iStock]["sales_stats_duration_3"] = result[ligne]["sales_stats_duration_3"];
                aOfStocks[iStock]["sales_stats_duration_all"] = result[ligne]["sales_stats_duration_all"];
                aOfStocks[iStock]["physical_quantity"] = result[ligne]["physical_quantity"];
                aOfStocks[iStock]["reserved_quantity"] = result[ligne]["reserved_quantity"];
                aOfStocks[iStock]["quantity"] = result[ligne]["quantity"];
                aOfStocks[iStock]["low_stock_threshold"] = result[ligne]["low_stock_threshold"];
                aOfStocks[iStock]["ideal_stock"] = result[ligne]["ideal_stock"];
                aOfStocks[iStock]["stock_ideal_conseille"] = result[ligne]["stock_ideal_conseille"];
                aOfStocks[iStock]["etat_stock_dans_1_mois"] = result[ligne]["etat_stock_dans_1_mois"];
                aOfStocks[iStock]["a_acheter_pour_avoir_bon_stock"] = result[ligne]["a_acheter_pour_avoir_bon_stock"];
                aOfStocks[iStock]["a_acheter_qui_sera_vendu_dans_1_mois"] = result[ligne]["a_acheter_qui_sera_vendu_dans_1_mois"];

                aOfStocks[iStock]["supplier_names"] = result[ligne]["supplier_names"].split(',');
                aOfStocks[iStock]["supplier_references"] = result[ligne]["supplier_references"].split(',');
                aOfStocks[iStock]["supplier_prices_te"] = result[ligne]["supplier_prices_te"].split(',');


                aOfStocks[iStock]["all"] = [];

                for (var supplier in aOfStocks[iStock]["supplier_prices_te"]) {
                    aOfStocks[iStock]["supplier_prices_te"][supplier] = parseFloat(aOfStocks[iStock]["supplier_prices_te"][supplier]).toFixed(2);
                    aOfStocks[iStock]["all"][supplier] = aOfStocks[iStock]["supplier_prices_te"][supplier] + "_" +  aOfStocks[iStock]["supplier_names"][supplier] + "_" + aOfStocks[iStock]["supplier_references"][supplier];
                }

                aOfStocks[iStock]["all"] = aOfStocks[iStock]["all"].sort();

                iStock++;
            }

            // INIT DATATABLE
            constructTableStocks();
            // reload datatable configuration
            tablesStocks = $('#datatable').DataTable(configuration);
            $('#divModalLoading').hide();
        })
        .fail(function(err) {
            // error
            alert('error : ' + err.status);
        });
}

// construit la table avec les données
function constructTableStocks() {
    var i;
    // tableau datatable
    var sHTML = "";

    sHTML += '<table id="datatable" class="table table-striped table-bordered" style="width:100%">';
    sHTML += "<thead>";
    sHTML += "<tr>";
    sHTML += "<th style='width: 30px; padding-right: 15px !important;'></th>";
    sHTML += "<th style='padding-right: 15px !important;'>ID</th>";
    sHTML += "<th>REFERENCE</th>";
    sHTML += "<th style='text-align:center;'>Image</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Nom</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Nb ventes<br/>sur 6 mois</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Nb ventes<br/>sur 2 mois</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Nb ventes<br/>sur 1 mois</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Nb ventes<br/>totales</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Qté physique</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Qté à expédier</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Qté disponible</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Niveau de stock bas</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Stock idéal</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>Etat stock dans 1 mois</th>";
    sHTML += "<th style='text-align:center;padding-right: 15px !important;'>A cmd pour stock 30j</th>";
    sHTML += "<th class='none'></th>";
    sHTML += "<th class='none'></th>";
    sHTML += "</tr>";
    sHTML += "</thead>";
    sHTML += "<tbody>";

    // pour chaque ligne de résultat, construire la ligne dans datatable
    for(i = 0; i < aOfStocks.length; i++) {

        sHTML += '<tr style="text-align:center;">';

        sHTML += '<td></td>';

        sHTML += '<th scope="row" style="text-align: center;"><a href="https://www.pincab.eu/admin797xdo6aw/index.php/sell/catalog/products/' + aOfStocks[i]["product_id"] + '?_token=' + tokenAdminProducts + '#tab-step6' + '" ' + 'target="_blank">' + aOfStocks[i]["product_id"] + '</a></th>';

        sHTML += "<td>" + aOfStocks[i]["product_reference"] + "</td>";

        sHTML += '<td>' + '<img src="https://www.pincab.eu/' + aOfStocks[i]["id_image"] + '-small_default/' + aOfStocks[i]["link_rewrite"] + '.jpg' + '" height="46px" width="46px">' + '</td>';

        if ((aOfStocks[i]["product_attribute_group_name"] !== "") && (aOfStocks[i]["product_attribute_name"] !== "")) {
            sHTML += "<td>" + aOfStocks[i]["product_name"] + "<br/><p style='font-weight: bold;'>" + aOfStocks[i]["product_attribute_group_name"] + " : " + aOfStocks[i]["product_attribute_name"] + "</p></td>";
        }
        else {
            sHTML += "<td>" + aOfStocks[i]["product_name"] + "</td>";
        }

        sHTML += "<td>" + aOfStocks[i]["sales_stats_duration_3"] + "</td>";

        sHTML += "<td>" + aOfStocks[i]["sales_stats_duration_2"] + "</td>";

        sHTML += "<td>" + aOfStocks[i]["sales_stats_duration_1"] + "</td>";

        sHTML += "<td>" + aOfStocks[i]["sales_stats_duration_all"] + "</td>";

        sHTML += "<td>" + aOfStocks[i]["physical_quantity"] + "</td>";

        sHTML += "<td>" + aOfStocks[i]["reserved_quantity"] + "</td>";

        if (parseInt(aOfStocks[i]["quantity"]) < parseInt(aOfStocks[i]["low_stock_threshold"])) {
            sHTML += '<td style="background-color: #CE5454">' + aOfStocks[i]["quantity"] + '</td>';
        }
        else if (parseInt(aOfStocks[i]["quantity"]) == parseInt(aOfStocks[i]["low_stock_threshold"])) {
            sHTML += '<td style="background-color: #ffc107">' + aOfStocks[i]["quantity"] + '</td>';
        }
        else {
            sHTML += "<td>" + aOfStocks[i]["quantity"] + "</td>";
        }

        sHTML += "<td>" + aOfStocks[i]["low_stock_threshold"] + "</td>";

        sHTML += "<td>" + aOfStocks[i]["ideal_stock"] + "</td>";

        sHTML += "<td>" + aOfStocks[i]["etat_stock_dans_1_mois"] + "</td>";

        if ((aOfStocks[i]["a_acheter_qui_sera_vendu_dans_1_mois"]) > 0) {
            sHTML += "<td>" + aOfStocks[i]["a_acheter_qui_sera_vendu_dans_1_mois"] + "</td>";
        }
        else {
            sHTML += "<td>" + '0' + "</td>";
        }

        sHTML += '<td>';

        sHTML += '<table class="table_date_add" style="table-layout: fixed; margin-top: -15px; margin-bottom: -15px; width: 1000px; border-bottom: 1px solid rgb(222, 226, 230); border-right: 1px solid rgb(222, 226, 230);">' + '<thead>' + '<tr  style="background-color:white !important;">' + '<th style="text-align: center;">Date d\'ajout</th>' + '<th style="text-align: center;">Référence produit</th>' + '</tr>' + '</thead>' + '<tbody>';

        var dateTime = new Date(aOfStocks[i]["date_add"]);
        dateTime = moment(dateTime).format("DD/MM/YYYY");

        sHTML += '<tr>' + '<td style="text-align: center;">' + dateTime + '</td>' + '<td style="text-align: center;">' + aOfStocks[i]["product_reference"] + '</td>' + '</tr>';

        sHTML += '</tbody>' + '</table>';

        sHTML += '</td>';

        sHTML += '<td>';

        if (aOfStocks[i]["supplier_names"] != "") {
            sHTML += '<table class="table_suppliers" style="table-layout: fixed; margin-top: -15px;  width: 1000px; border-bottom: 1px solid rgb(222, 226, 230); border-right: 1px solid rgb(222, 226, 230); margin-bottom: 5px;">' + '<thead>' + '<tr  style="background-color:white !important;">' + '<th style="text-align: center;">Nom fournisseur</th>' + '<th style="text-align: center;">Référence fournisseur</th>' + '<th style="text-align: center;">Prix d\'achat</th>' + '</tr>' + '</thead>' + '<tbody>';

            for (var supplier in aOfStocks[i]["all"]) {

                aOfStocks[i]["all"][supplier] =  aOfStocks[i]["all"][supplier].split("_");

                sHTML += '<tr>' + '<td style="text-align: center;">' + aOfStocks[i]["all"][supplier][1] + '</td>' + '<td style="text-align: center;">' + aOfStocks[i]["all"][supplier][2] + '</td>' + '<td style="text-align: center;">' + aOfStocks[i]["all"][supplier][0] + ' €' + '</td>' + '</tr>';
            }

            sHTML += '</tbody>' + '</table>';
        }

        sHTML += '</td>';
    }
    sHTML += "</tbody>";
    sHTML += "</table>";
    $('#manage_stock_table').html(sHTML);

    // Handle click on "Expand All" button
    $('#btn-show-all-children').on('click', function(){
        // Expand row details
        table.rows(':not(.parent)').nodes().to$().find('td:first-child').trigger('click');
    });

    // Handle click on "Collapse All" button
    $('#btn-hide-all-children').on('click', function(){
        // Collapse row details
        table.rows('.parent').nodes().to$().find('td:first-child').trigger('click');
    });
}

/**
 * clear HTML table
 * clear and destroy datatable
 * build table and call database
 */
function rebuildDatatableStocks() {
    tablesStocks.clear();
    tablesStocks.destroy();
    constructTableStocks();
    tablesStocks = $('#datatable').DataTable(configuration);
}

// datatable configuration
// order : tri selon une colonne (0 = 1ère colonne) par ordre croissant (asc) ou décroissant (desc)
// pageLength : nombre de résultats affichés par défaut
// lenghtMenu : choix du nombre de résultats à afficher
// language : traduction
// columns :
// orderable : triable (true ou false)
// visible : colonne affichée ou non (ID par exemple, ou date (si on veut trier par la date par exemple)) (true ou false)
// searchable : filtrable par le champ de recherche
const configuration = {
    "stateSave": false,
    "order": [[1, "asc"]],
    "pageLength": 25,
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
    "language" : {
        "info": "Affichage des lignes _START_ &agrave; _END_ sur _TOTAL_ r&eacute;sultats au total",
        "emptyTable": "Aucun r&eacute;sultat disponible",
        "lengthMenu": "Affichage de _MENU_ r&eacute;sultats",
        "search" : "Rechercher : ",
        "zeroRecords" : "Aucun r&eacute;sultat trouv&eacute;",
        "paginate" : {
            "previous": "Pr&eacute;c&eacute;dent",
            "next": "Suivant"
        },
        "sInfoFiltered": "(filtr&eacutes &agrave; partir de _MAX_ r&eacute;sultats au total)",
        "sInfoEmpty": "Aucun r&eacute;sultat disponible"
    },
    "dom": 'flrtip',
    "columns": [
        {
            "orderable": false
        },
        {
            "orderable": true
        },
        {
            "visible": false
        },
        {
            "orderable": false
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        }
    ],
    "retrieve": true,
    "responsive": true,
    "autoWidth": false
};

/**
 * Init start
 *
 */
// une fois la page chargée, charger les stocks
$(document).ready(function() {
    loadTokenAdminProducts();
    loadStocks();
});