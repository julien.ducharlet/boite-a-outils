<?php

class BoiteAOutils extends Module
{
    public $context;
    public function __construct()
    {
        $this->name = 'boiteaoutils';
        $this->author = 'Piment Bleu';
        $this->tab = 'content_management';
        $this->version = '0.1.0';
        parent::__construct();

        $token = Tools::getValue("token", "");
        $this->baseParams = "?controller=AdminModules&configure=boiteaoutils&token=" . $token;
        $this->page = basename(__FILE__, '.php');

        $this->displayName = $this->l('Boite a outils');
        $this->description = $this->l('Boite a outils');
        $this->confirmUninstall = $this->l('Etes vous sur de vouloir desinstaller ce module ?');
    }

    public function install()
    {
        if (
            !parent::install()
            || !$this->createTab()
            )
        {
            $this->createConfigIni();
            return true;
        }
        return true;
    }

    public function uninstall()
    {
        $tab = new Tab(Tab::getIdFromClassName('AdminBoiteAOutils'));
        if (!$tab->delete()) {
            return false;
        }
        if (!parent::uninstall()) {
            return false;
        }
        return true;
    }

    public function createConfigIni() {
        $included_files = get_included_files();
        $path_parts = explode("/", $included_files[0]);
        $i = 0;
        $path = "";

        while ($i < (count($path_parts)-2))  {
            $path .= $path_parts[$i] . "/";
            $i++;
        }

        $PATH_HOME = $path . "modules/boiteaoutils/";

        $parameters_file = $path . "app/config/parameters.php";

        $config_ini_file = $PATH_HOME . "files/config_bao.ini";

        $lines = file($parameters_file);

        $parameters = [];
        $parameters_key = [];
        $parameters_value = [];
        for ($i = 0; $i < count($lines); $i++) {
            $parameters[$i] = explode('=>', $lines[$i]);
            $parameters_key[$i] = $parameters[$i][0];
            $parameters_value[$i] = $parameters[$i][1];
        }
        $DB_HOST = str_replace("'", "", $parameters_value[3]);
        $DB_HOST = trim($DB_HOST);
        $DB_HOST = str_replace(",", "\n", $DB_HOST);

        $DB_LOGIN = str_replace("'", "", $parameters_value[6]);
        $DB_LOGIN = trim($DB_LOGIN);
        $DB_LOGIN = str_replace(",", "\n", $DB_LOGIN);

        $DB_PSW = str_replace("'", "", $parameters_value[7]);
        $DB_PSW = trim($DB_PSW);
        $DB_PSW = str_replace(",", "\n", $DB_PSW);

        $DB_NAME = str_replace("'", "", $parameters_value[5]);
        $DB_NAME = trim($DB_NAME);
        $DB_NAME = str_replace(",", "\n", $DB_NAME);

        $DB_PREFIX = str_replace("'", "", $parameters_value[8]);
        $DB_PREFIX = trim($DB_PREFIX);
        $DB_PREFIX = str_replace(",", "\n", $DB_PREFIX);

        $write = "[PATHS]" . "\n";
        $write .= "PATH_HOME  = " . $PATH_HOME . "\n";
        $write .= "\n";
        $write .= "[DATABASE]" . "\n";
        $write .= "DB_HOST  = " . $DB_HOST;
        $write .= "DB_LOGIN = " . $DB_LOGIN;
        $write .= "DB_PSW   = " . "'" . $DB_PSW . "'";
        $write .= "DB_NAME  = " . $DB_NAME;
        $write .= "DB_PREFIX  = " . $DB_PREFIX;

        $ini = fopen($config_ini_file, "w") or die("Unable to open file!");
        fwrite($ini, $write);
        fclose($ini);
    }

    private function createTab()
    {
        $this->context = Context::getContext();

        $this->context->controller->addJS(__PS_BASE_URI__.'modules/' . $this->name . '/views/js/loader/jquery.loader-min.js');
        $this->context->controller->addCSS(__PS_BASE_URI__.'modules/' . $this->name . '/views/css/admin.css', 'all');

        $tab = new Tab();
        $tab->class_name = 'AdminBoiteAOutils';
        $adminModuleParentTab = Tab::getInstanceFromClassName('AdminModules');
        $tab->id_parent = (int)$adminModuleParentTab->id_parent;
        $tab->module = $this->name;
        $tab->name[Configuration::get('PS_LANG_DEFAULT')] = 'Boite à outils';
        $tab->name[Language::getIdByIso('en')] = 'Boite à outils';
        $tab->name[Language::getIdByIso('fr')] = 'Boite à outils';
        $tab->add();

        $sql = 'SELECT id_quick_access AS id FROM `' . _DB_PREFIX_ . 'quick_access` q WHERE q.`link` LIKE \'%AdminBoiteAOutils%\'';
        $result = Db::getInstance()->getRow($sql);

        if (empty($result)) {
            $quickAccess = new QuickAccess();
            $tmp = array();
            $languages = Language::getLanguages();
            foreach ($languages AS $lang) {
                $tmp[$lang['id_lang']] = "Boite a outils";
            }
            $quickAccess->name = $tmp;
            $quickAccess->link = "index.php?controller=AdminBoiteAOutils";
            $quickAccess->new_window = true;
            $quickAccess->add();
        }
    }
}